/** @file stellar_assembly_unique_ids.cpp
 * @brief For an existing stellar assembly catalog, determine the
 *        (unique) SubhaloID of the merger tree "leaf" in which each
 *        stellar particle formed.
 *
 * @author Vicente Rodriguez-Gomez (vrodgom.astro@gmail.com)
 */

#include <vector>
#include <string>
#include <iomanip>
#include <fstream>
#include <sstream>

#include "../InputOutput/ReadSubfindHDF5.hpp"
#include "../InputOutput/ReadTreeHDF5.hpp"
#include "../InputOutput/GeneralHDF5.hpp"
#include "../Util/GeneralUtil.hpp"
#include "../Util/TreeUtil.hpp"


/** @brief Get the "main leaf subhalo ID" for each stellar particle. */
void stellar_assembly_unique_ids(const std::string& basedir,
    const std::string& treedir, const std::string& assemblydir,
    const std::string& writepath, const snapnum_type snapnum) {

  // Load merger tree
  WallClock wall_clock;
  std::cout << "Loading merger tree...\n";
  int filenum = -1;  // read from all merger tree files
  std::string name = "tree_extended";  // Full format
  Tree tree(treedir, name, filenum);
  std::cout << "Loaded merger tree. Total time: " <<
      wall_clock.seconds() << " s.\n";
  std::cout << "\n";

  // Open output file
  std::stringstream tmp_stream;
  tmp_stream << writepath << "_" <<
      std::setfill('0') << std::setw(3) << snapnum << ".hdf5";
  std::string writefilename = tmp_stream.str();
  H5::H5File writefile(writefilename, H5F_ACC_TRUNC);

  // Only proceed if there is at least one subhalo
  auto nsubs = subfind::get_scalar_attribute<uint32_t>(
      basedir, snapnum, "Nsubgroups_Total");
  if (nsubs == 0) {
    std::cout << "No subhalos in snapshot " << snapnum << ". Skipping...\n";
    writefile.close();
    return;
  }

  // Read some data from stellar assembly catalog
  std::cout << "Reading info from stellar assembly catalog...\n";
  wall_clock.start();
  tmp_stream.str("");
  tmp_stream << assemblydir << "/stars_" <<
      std::setfill('0') << std::setw(3) << snapnum << ".hdf5";
  std::string assemb_filename = tmp_stream.str();
  auto SubfindIDAtFormation = read_dataset<index_type>(
      assemb_filename, "SubfindIDAtFormation");
  auto SnapNumAtFormation = read_dataset<snapnum_type>(
      assemb_filename, "SnapNumAtFormation");
  uint64_t nparts = SubfindIDAtFormation.size();
  std::cout << "Time: " << wall_clock.seconds() << " s.\n";

  // Store data here:
  std::vector<uint64_t> LeafSubhaloID(nparts, -1);

  uint64_t num_formed_outside_subhalos = 0;

  // Iterate over stellar particles
  wall_clock.start();
  std::cout << "Iterating over stellar particles...\n";
  for (uint64_t pos = 0; pos < nparts; ++pos) {
    // Skip stellar particles that were formed outside of subhalos.
    if (SubfindIDAtFormation[pos] == -1) {
      ++num_formed_outside_subhalos;
      continue;
    }
    auto sub = tree.subhalo(SnapNumAtFormation[pos],
                            SubfindIDAtFormation[pos]);
    LeafSubhaloID[pos] = sub.data().MainLeafProgenitorID;
  }
  std::cout << "Time: " << wall_clock.seconds() << " s.\n";
  std::cout << num_formed_outside_subhalos << " of " << nparts <<
      " stellar particles were formed outside of subhalos." << std::endl;

  // Write to file.
  wall_clock.start();
  std::cout << "Writing to output file...\n";
  add_array(writefile, LeafSubhaloID, "LeafSubhaloID", H5::PredType::NATIVE_UINT64);
  std::cout << "Time: " << wall_clock.seconds() << " s.\n";
  // Close (and flush) file
  writefile.close();
  std::cout << "Finished for snapshot " << snapnum << std::endl;

}


int main(int argc, char** argv)
{
  // Check input arguments
  if (argc != 6) {
    std::cerr << "Usage: " << argv[0] << " basedir treedir assemblydir" <<
        " writepath snapnum\n";
    exit(1);
  }

  // Read input
  std::string basedir(argv[1]);
  std::string treedir(argv[2]);
  std::string assemblydir(argv[3]);
  std::string writepath(argv[4]);
  snapnum_type snapnum = atoi(argv[5]);

  // Measure CPU and wall clock (real) time
  WallClock wall_clock;
  CPUClock cpu_clock;

  // Do stuff
  stellar_assembly_unique_ids(basedir, treedir, assemblydir, writepath, snapnum);

  // Print wall clock time and speedup
  std::cout << "Time: " << wall_clock.seconds() << " s.\n";
  std::cout << "Speedup: " << cpu_clock.seconds()/wall_clock.seconds() << ".\n";

  return 0;
}
